#pragma once

namespace ZarateEngine{
	class Application
	{
	public:
		Application();
		virtual ~Application();
		void Run();
	};
}


